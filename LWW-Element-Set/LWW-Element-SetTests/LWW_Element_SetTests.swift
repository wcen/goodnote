//
//  LWW_Element_SetTests.swift
//  LWW-Element-SetTests
//
//  Created by Weixiong Cen on 5/12/20.
//

import XCTest
@testable import LWW_Element_Set

class LWW_Element_SetTests: XCTestCase {
    
    var set: LWWElementSet<String>!
    
    var now: TimeInterval {
        get {
            return Date().timeIntervalSince1970
        }
    }
    
    override func setUp() {
        super.setUp()
        set = LWWElementSet<String>()
    }

    /**
    Testing addition to the set
    */
    func testAdd() {
        let element = "a"
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        XCTAssertTrue(set.removeSet.isEmpty)
    }
    
    /**
    Testing removal to the set
    */
    func testsRemove() {
        let element = "a"
        
        XCTAssertTrue(set.removeSet.isEmpty)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        XCTAssertTrue(set.addSet.isEmpty)
    }
    
    /**
    Testing the timestamp of addition is updated with the later add timestamp
    */
    func testAddTimeUpdatedForLaterAddTime() {
        let element = "a"
        let addTimestamp = now
        let addTimestampLater = addTimestamp + 10
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: addTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.add(element, timestamp: addTimestampLater)
        XCTAssertTrue(set.addSet.count == 1)
        XCTAssertNotEqual(addTimestamp, set.lookup(element))
    }
    
    /**
     Testing the timestamp of addition is not updated if the new addition timestamp is ealier
    */
    func testAddTimeNotUpdatedForEarlierAddTime() {
        let element = "a"
        let addTimestamp = now
        let addTimestampEarlier = addTimestamp - 10
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: addTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.add(element, timestamp: addTimestampEarlier)
        XCTAssertTrue(set.addSet.count == 1)
        XCTAssertEqual(addTimestamp, set.lookup(element))
    }
    
    /**
     Testing addSet and removeSet after addtion and removal
    */
    func testAddAndRemove() {
        let element = "a"
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        XCTAssertTrue(set.removeSet.isEmpty)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.add(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        XCTAssertTrue(set.addSet.count == 1)
    }
    
    /**
     Testing multiple addtion with different element
    */
    func testAddMultiple() {
        XCTAssertTrue(set.addSet.isEmpty)
        set.add("a", timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.add("b", timestamp: now)
        XCTAssertTrue(set.addSet.count == 2)
    }
    
    /**
     Testing multiple removal with different element
    */
    func testRemoveMultiple() {
        XCTAssertTrue(set.addSet.isEmpty)
        set.remove("a", timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        set.remove("b", timestamp: now)
        XCTAssertTrue(set.removeSet.count == 2)
    }
    
    /**
     Testing merge result of adding 1 element
    */
    func testMergeAdd() {
        let element = "a"
        let nowTimestamp = now
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.count == 1)
        XCTAssertEqual(merge[element], nowTimestamp)
    }
    
    /**
     Testing merge result of removing 1 element without adding any element
    */
    func testMergeRemove() {
        let element = "a"
        
        XCTAssertTrue(set.removeSet.isEmpty)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.isEmpty)
    }
    
    /**
     Testing the timestamp of merge result is updated with the later add timestamp
    */
    func testMergeAddTimeUpdate() {
        let element = "a"
        let nowTimestamp = now
        let laterTimestamp = now + 10
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        set.add(element, timestamp: laterTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.count == 1)
        XCTAssertEqual(merge[element], laterTimestamp)
    }
    
    /**
     Testing the timestamp of merge result is not updated if the new addition timestamp is ealier
    */
    func testMergeAddTimeNotUpdate() {
        let element = "a"
        let nowTimestamp = now
        let earlierTimestamp = now - 10
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        set.add(element, timestamp: earlierTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.count == 1)
        XCTAssertEqual(merge[element], nowTimestamp)
    }
    
    /**
     Testing merge result of adding then removing element
    */
    func testMergeAddAndRemove() {
        let element = "a"
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        XCTAssertTrue(set.addSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.isEmpty)
    }
    
    /**
     Testing merge result of remove element before adding
    */
    func testMergeRemoveBeforeAdd() {
        let element = "a"
        
        XCTAssertTrue(set.removeSet.isEmpty)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        XCTAssertTrue(set.removeSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.count == 1)
    }
    
    /**
     Testing merge result of adding an element, then remove an element, then adding again
    */
    func testMergeAddRemoveAdd() {
        let element = "a"
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        XCTAssertTrue(set.addSet.count == 1)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.count == 1)
    }
    
    /**
     Testing merge result of adding multiple elements
    */
    func testMergeAddMultiple() {
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add("a", timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.add("b", timestamp: now)
        XCTAssertTrue(set.addSet.count == 2)
        
        let merge = set.merge()
        XCTAssertTrue(merge.count == 2)
    }
    
    /**
     Testing merge result when add and remove timestamp are the same
    */
    func testMergeBias() {
        let element = "a"
        let nowTimestamp = now
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.remove(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.removeSet.count == 1)
        
        let merge = set.merge()
        XCTAssertTrue(merge.count == 1)
    }
    
    /**
     Testing lookup result after adding 1 element
    */
    func testLookupAdd() {
        let element = "a"
        let nowTimestamp = now
        
        XCTAssertNil(set.lookup(element))
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        XCTAssertNotNil(set.lookup(element))
        XCTAssertEqual(set.lookup(element), nowTimestamp)
    }
    
    /**
     Testing lookup result after removing 1 element without adding any
    */
    func testLookupRemove() {
        let element = "a"
        
        XCTAssertTrue(set.removeSet.isEmpty)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        XCTAssertNil(set.lookup(element))
    }
    
    /**
     Testing lookup result timestamp is updated with the later add timestamp
    */
    func testLookupAddTimeUpdate() {
        let element = "a"
        let nowTimestamp = now
        let laterTimestamp = now + 10
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        set.add(element, timestamp: laterTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        XCTAssertNotNil(set.lookup(element))
        XCTAssertEqual(set.lookup(element), laterTimestamp)
    }
    
    /**
     Testing lookup result timestamp is not updated with the ealier add timestamp
    */
    func testLookupAddTimeNotUpdate() {
        let element = "a"
        let nowTimestamp = now
        let earlierTimestamp = now - 10
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: nowTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        set.add(element, timestamp: earlierTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        XCTAssertNotNil(set.lookup(element))
        XCTAssertEqual(set.lookup(element), nowTimestamp)
    }
    
    /**
     Testing lookup result of adding then removing element
    */
    func testLookupAddAndRemove() {
        let element = "a"
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        XCTAssertTrue(set.addSet.count == 1)
        
        XCTAssertNil(set.lookup(element))
    }
    
    /**
     Testing lookup result of remove is ealier than add time
    */
    func testLookupRemoveBeforeAdd() {
        let element = "a"
        
        XCTAssertTrue(set.removeSet.isEmpty)
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        XCTAssertTrue(set.removeSet.count == 1)
        
        XCTAssertNotNil(set.lookup(element))
    }
    
    /**
     Testing lookup result of adding an element, then remove an element, then adding again
    */
    func testLookupAddRemoveAdd() {
        let element = "a"
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.remove(element, timestamp: now)
        XCTAssertTrue(set.removeSet.count == 1)
        
        XCTAssertTrue(set.addSet.count == 1)
        set.add(element, timestamp: now)
        XCTAssertTrue(set.addSet.count == 1)
        
        XCTAssertNotNil(set.lookup(element))
    }
    
    /**
     Testing lookup result of adding multiple elements
    */
    func testLookupAddMultiple() {
        let elementA = "a"
        let elementB = "b"
        let firstAddTimestamp = now
        let secondAddTimestamp = firstAddTimestamp + 10
        
        XCTAssertTrue(set.addSet.isEmpty)
        set.add(elementA, timestamp: firstAddTimestamp)
        XCTAssertTrue(set.addSet.count == 1)
        
        set.add(elementB, timestamp: secondAddTimestamp)
        XCTAssertTrue(set.addSet.count == 2)
        XCTAssertNotNil(set.lookup(elementA))
        XCTAssertNotNil(set.lookup(elementB))
        XCTAssertNotEqual(set.lookup(elementA), set.lookup(elementB))
    }
}
