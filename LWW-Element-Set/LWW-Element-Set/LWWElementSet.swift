//
//  LWWElementSet.swift
//  LWW-Element-Set
//
//  Created by Weixiong Cen on 5/12/20.
//

import Foundation

public struct LWWElementSet<T: Hashable & Equatable > {
    var addSet = [T: TimeInterval]()
    var removeSet = [T: TimeInterval]()
    
    mutating func add(_ element: T,
             timestamp: TimeInterval) {
        if let previousAddTime = addSet[element], previousAddTime > timestamp {
            return
        }
        addSet[element] = timestamp
    }
    
    mutating func remove(_ element: T,
                          timestamp: TimeInterval) {
        if let previousRemoveTime = removeSet[element], previousRemoveTime > timestamp {
            return
        }
        removeSet[element] = timestamp
    }
    
    func merge() -> [T: TimeInterval] {
        return addSet.filter { (key: T, value: TimeInterval) -> Bool in
            if let removeTimestamp = removeSet[key], removeTimestamp > value  {
                return false
            }
            return true
        }
    }
    
    func lookup(_ element: T) -> TimeInterval? {
        return merge()[element]
    }
}
